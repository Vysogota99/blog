class Post < ApplicationRecord
  has_many :comments
  belongs_to :user

  paginates_per 10

  def body
    annotation.present? ? annotation.truncate(200) : '' 
  end
end