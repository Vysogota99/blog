class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post

  def timestamp
    created_at.strftime('%H:%M:%S %d %B %Y')
  end
end