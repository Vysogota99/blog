# frozen_string_literal: true

class Report < ApplicationRecord
  module Types
    PORTFOLIO_API = 'portfolio_api'
    PORTFOLIO_XLSX = 'portfolio_xlsx'
    def self.all
      [
        PORTFOLIO_API,
        PORTFOLIO_XLSX
      ]
    end
  end

  module Statuses
    CREATED             = 'created'
    RUNNING             = 'running'
    FINISHED_SUCCESS    = 'finished_success'
    FINISHED_WITH_ERROR = 'finished_with_error'

    def self.all
      [
        CREATED,
        RUNNING,
        FINISHED_SUCCESS,
        FINISHED_WITH_ERROR
      ]
    end
  end

  enum report_type: [Types::PORTFOLIO_API, Types::PORTFOLIO_XLSX]
  enum status: [Statuses::CREATED, Statuses::RUNNING, Statuses::FINISHED_SUCCESS, Statuses::FINISHED_WITH_ERROR]

  belongs_to :user
  has_many :positions, dependent: :destroy
  has_many :operations, dependent: :destroy
  has_many :candles, dependent: :destroy

  def interval
    "#{start_date} - #{end_date}"
  end

  def failed?
    status == Statuses::FINISHED_WITH_ERROR
  end

  def start_date_s
    start_date.strftime("%d %B %Y")
  end

  def end_date_s
    end_date.strftime("%d %B %Y ")
  end
end
