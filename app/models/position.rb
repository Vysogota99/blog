# frozen_string_literal: true

class Position < ApplicationRecord
  module Types
    STOK      = 'Stock'
    ETF       = 'Etf'
    CURRENCY  = 'Currency'
  end

  belongs_to :report
end
