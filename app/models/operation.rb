# frozen_string_literal: true

class Operation < ApplicationRecord
  include ActiveModel::Serialization

  paginates_per 20

  belongs_to :report

  has_many :trades
  accepts_nested_attributes_for :trades

  has_one :commission
  accepts_nested_attributes_for :commission

  def attributes
    {
      'operation_type' => nil,
      'date' => nil,
      'is_margin_call' => nil,
      'instrument_type' => nil,
      'figi' => nil,
      'quantity' => nil,
      'quantity_executed' => nil,
      'price' => nil,
      'payment' => nil,
      'currency' => nil,
      'status' => nil
    }
  end

  def self.attributes
    %w[ operation_type date is_margin_call
        instrument_type figi quantity quantity_executed
        price payment currency status trades_attributes
        commission_attributes ]
  end
end
