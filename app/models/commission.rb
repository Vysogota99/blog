# frozen_string_literal: true

class Commission < ApplicationRecord
  belongs_to :operation
end
