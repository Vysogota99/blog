class ReportsController < AuthorizeController
  before_action :find_reports, only: %i[index]
  before_action :find_report, only: %i[destroy show]

  # rescue_from Reports::Tinkoff::Errors::ApiTokenBlankError, with: :report_failed
  # rescue_from Reports::Tinkoff::Errors::ReportBuildingError, with: :report_failed

  def index; end

  def new
    @report = current_user.reports.new(
      status: Report::Statuses::CREATED,
      start_date: Date.today - 1.year,
      end_date: Date.today
    )
  end

  def create
    if (report = current_user.reports.create(report_params))
      Reports::Tinkoff::Builder.call(report, extra_metadata)

      flash[:success] = 'Начало формирования отчета'
    else
      flash[:warning] = 'Не удалось начать формирование отчета'
    end

    redirect_to action: 'index'
  end

  def show
    @operations = @report.operations
  end

  def destroy
    @report.destroy
  end

  private

  def find_reports
    @reports = current_user.reports.order(created_at: :desc)
  end

  def find_report
    @report = current_user.reports.find(params[:id])
  end

  def report_params
    params.require(:report).permit(:report_type, :status, :start_date, :end_date)
  end

  def extra_metadata
    metadata = {}
    metadata[:broker_report] = params[:report][:broker_report] if params[:report][:broker_report].present?
    metadata
  end

  def report_failed
    redirect_to action: :index
  end
end
