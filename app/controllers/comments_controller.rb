class CommentsController < AuthorizeController
  before_action :find_post
  def create
    comment = @post.comments.new(comment_params)
    comment.user_id = current_user.id

    if comment.save
      flash[:success] = 'Комментарий добавлен'
      comments_count = comment.post.reload.comments.count
      ActionCable.server.broadcast('comments', { text: comment.text, user_name: comment.user.name, timestamp: comment.timestamp,
                                                 post_id: @post.id, comments_count: comments_count })
      NotificationsChannel.broadcast_to(comment.post.user, message: 'К вашему посту добавлен новый комментарий')
    else
      flash[:warning] = 'Не удалось добавить комментарий'
    end

    redirect_back fallback_location: posts_url
  end

  private

  def comment_params
    params.require(:comment).permit(:text)
  end

  def find_post
    @post = Post.find(params[:post_id])
  end
end
