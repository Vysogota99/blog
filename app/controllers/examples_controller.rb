# frozen_string_literal: true

# ExamplesController
class ExamplesController < AuthorizeController
  def show; end

  def index; end
end
