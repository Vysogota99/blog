# frozen_string_literal: true

# MainPageController
class HomeController < ApplicationController
  def index; end
end
