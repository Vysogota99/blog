class PostsController < AuthorizeController
  before_action :find_posts, only: [:index]
  before_action :find_post, only: %i[show edit update]
  before_action :check_permissions, only: %i[edit]

  def index; end

  def new
    @post = Post.new(title: 'Пример заголовка')
  end

  def edit; end

  def show; end

  def update
    if @post.update(post_params)
      flash[:success] = 'Пост обновлен'
      redirect_to action: :show
    else
      flash[:warning] = 'Не удалось обновить пост'
    end
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      flash[:success] = 'Пост создан'
      redirect_to action: :index
    else
      flash[:warning] = 'Не удалось сохранить пост'
    end
  end

  private

  def find_posts
    @posts = params[:my] ? current_user.posts.page(params[:page] || 1) : Post.page(params[:page] || 1)
  end

  def find_post
    @post = Post.find(params[:id])
  rescue
    render_404
  end

  def post_params
    update_post_params.require(:post).permit(:title, :annotation, :introduction,
                                             :literature_review, :main_part, :conclusions,
                                             bibliography: [], keywords: [])
  end

  def update_post_params
    rework_params = params
    rework_params[:post][:bibliography] = rework_params[:post][:bibliography].split(' ')
    rework_params[:post][:keywords] = rework_params[:post][:keywords].split(' ')
    rework_params
  end

  def check_permissions
    redirect_to(action: :index) unless current_user.id == @post.user_id
  end
end
