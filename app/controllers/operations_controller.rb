# frozen_string_literal: true

class OperationsController < AuthorizeController
  before_action :find_report
  protect_from_forgery except: :index

  def show; end

  def index
    respond_to do |format|
      format.js do
        @operations = @report.operations.page(params[:page] || 1)
        render 'reports/operations/index'
      end
    end
  end

  private

  def find_report
    @report = Report.find(params[:report_id])
  end
end
