# frozen_string_literal: true

class AuthorizeController < ApplicationController
  before_action :authenticate_user!
end
