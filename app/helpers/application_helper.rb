# frozen_string_literal: true

module ApplicationHelper
  def gravatar_for(user, opts = {})
    opts[:alt] = user.name
    image_tag "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(user.email)}?s=#{opts.delete(:size) { 40 }}",
              opts
  end

  def report_options
    Report::Types.all.map { |type| [t("report.types.#{type}"), type] }
  end
end
