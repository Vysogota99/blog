import consumer from "./consumer"

consumer.subscriptions.create({ channel: 'CommentsChannel' }, {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    this.appendComment(data)
    this.updateCommentsCounter(data)
  },

  appendComment(data) {
    const html = this.createComment(data)
    const element = document.querySelector("[data-post-id='" + data['post_id'] + "']")
    if(element == null) {
      return
    }

    element.insertAdjacentHTML("beforeend", html)
  },

  updateCommentsCounter(data) {
    const new_value = data['comments_count']
    const element =  document.querySelector("[data-stats-for-post='" + data['post_id'] + "'] [rel=comments]")
    if(element == null) {
      return
    }
    element.innerText = new_value
  },

  createComment(data) {
    return `
      <li class="list-group-item">
      <div class="card">
        <div class="card-body">
          <blockquote class="blockquote mb-0">
            <p>
              ${data["text"]}
            </p>
            <footer class="blockquote-footer">
              ${data["user_name"]}
              ${data["timestamp"]}
            </footer>
          </blockquote>
        </div>
      </div
    </li>
    `
  }
});
