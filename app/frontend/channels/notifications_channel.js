import consumer from "./consumer"
import notifier from "./services/notifier"

consumer.subscriptions.create({ channel: 'NotificationsChannel' }, {
  connected() {
    // Called when the subscription is ready for use on the server
  },
  received(data) {
    notifier.notify(data.message)
  }

})