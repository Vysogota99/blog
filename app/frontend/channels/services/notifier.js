import {v4 as uuidv4} from 'uuid'

export default new class {
  constructor() {
  }

  html(message, uid) {
    return `
      <div class="alert alert-primary" role="alert" id="alert_${uid}">
        ${message}
      </div>
    `
  }

  notify(message) {
    const uid = uuidv4()
    const html = this.html(message, uid)
    const element = document.querySelector('.alerts')
    if(element == null) {
      return
    }

    element.insertAdjacentHTML("afterbegin", html)
    setTimeout(this.destroy_element, 2000, uid);
  }

  destroy_element(uid) {
    document.getElementById(`alert_${uid}`).remove()
  }
}
