class ApplicationService
  def self.call(*args, &block)
    new(*args, &block).call
  end

  def self.async_call(*args, &block)
    new(*args, &block).async
  end
end
