# frozen_string_literal: true

module Reports
  module Tinkoff
    # Portfolio - build portfolio report by getting data from Tinkoff API
    class PortfolioXlsx < Builder
      def initialize(report, extra_payload)
        super
      end

      def call
        portfolio
      end

      private

      def portfolio
        byebug
        broker_report = @extra_payload[:broker_report]
        Roo::Spreadsheet.open(broker_report)
      end
    end
  end
end
