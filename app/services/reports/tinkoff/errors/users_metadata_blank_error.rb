# frozen_string_literal: true

module Reports
  module Tinkoff
    module Errors
      class UsersMetadataBlankError < Reports::Errors::StandardError
        def initialize(msg, report_id)
          super
        end
      end
    end
  end
end
