# frozen_string_literal: true

module Reports
  module Tinkoff
    module Errors
      class ReportBuildingError < Reports::Errors::StandardError
        attr_reader :report_id

        def initialize(msg, report_id, backtrace)
          super(msg, report_id)
          set_backtrace(backtrace)
        end
      end
    end
  end
end
