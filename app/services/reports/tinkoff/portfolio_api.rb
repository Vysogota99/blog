# frozen_string_literal: true

module Reports
  module Tinkoff
    # PortfolioAPI - build portfolio report by getting data from Tinkoff API
    class PortfolioApi < Builder
      def initialize(report, extra_payload)
        super
        raise Errors::ApiTokenBlankError.new('У пользователя не заполнены данные', report.id) if report.user.users_metadata.blank?

        @api_token = @report.user.users_metadata.tinkoff_token
        raise Errors::ApiTokenBlankError if @api_token.blank?
        @figies = {}
      end

      def call
        @report.update(status: Report::Statuses::RUNNING)
        operations
        history_data
        @report.update(status: Report::Statuses::FINISHED_SUCCESS)
      rescue StandardError => e
        exception = Errors::ReportBuildingError.new('Ошибка формирования отчета', @report.id, e.backtrace)
        @report.update(status: Report::Statuses::FINISHED_WITH_ERROR)
        raise exception
      end

      private

      def operations
        response = Reports::Tinkoff::RestClient.new(@api_token).operations(@report.start_date, @report.end_date)
        Reports::Tinkoff::RestClient.process_request(response)
        body = response.body
        @operations = body['payload']['operations'].map do |operation|
          operation.deep_transform_keys!(&:underscore)
          operation.transform_keys! { |key| %w[trades commission].include?(key) ? "#{key}_attributes" : key }
          operation.slice!(*Operation.attributes)
          operation = @report.operations.new(operation)
          operation.save
          figi(operation)
        end
      end

      def figi(operation)
        figi_key = operation.figi
        return if figi_key.blank?

        figi_date = operation.date

        if @figies[figi_key].present?
          @figies[figi_key][:from] = figi_date if @figies[figi_key][:from] > figi_date
        else
          @figies[figi_key] = { from: figi_date }
        end
      end

      def history_data
        @figies.each do |figi_name, figi_data|
          response = Reports::Tinkoff::RestClient.new(@api_token).candles_for_figi(figi_name, figi_data[:from], Date.today)
          Reports::Tinkoff::RestClient.process_request(response)
          body = response.body
          @report.candles.create(body['payload']['candles'])
        end
      end
    end
  end
end
