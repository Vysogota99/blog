# frozen_string_literal: true

module Reports
  module Tinkoff
    # Builder build report
    class Builder < ApplicationService
      attr_reader :report

      def initialize(report, extra_payload)
        @report = report
        @extra_payload = extra_payload
      end

      def call
        case report.report_type
        when Report::Types::PORTFOLIO_API
          Reports::Tinkoff::PortfolioApi.call(@report, @extra_payload)
        when Report::Types::PORTFOLIO_XLSX
          Reports::Tinkoff::PortfolioXLSX.call(@report, @extra_payload)
        end
      end
    end
  end
end
