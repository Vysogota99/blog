# frozen_string_literal: true

require 'async/barrier'
require 'async/http/internet'

module Reports
  module Tinkoff
    # Client for http integration with Tinkoff API
    class RestClient
      module Routes
        PORTFOLIO = '/openapi/portfolio'
        OPERATIONS = '/openapi/operations'
        CANDLES = '/openapi/market/candles'
      end

      DEFAULT_INTERVAL = 'day'
      TINKOFF_API_URL = 'https://api-invest.tinkoff.ru'

      def initialize(api_token, portfolio_id = '')
        @api_token    = api_token
        @portfolio_id = portfolio_id

        configure_connection
      end

      def portfolio
        log_tagger.tagged(%w[TINKOFF API_INVEST]) do
          @conn.get(Routes::PORTFOLIO)
        end
      end

      # operations see docs https://tinkoffcreditsystems.github.io/invest-openapi/swagger-ui/#/operations/get_operations.
      # from  - DataeTime
      # to    - DateTime
      # figi  - String
      def operations(from, to, figi = '')
        request_params = { from: from.rfc3339, to: to.rfc3339 }
        request_params[:figi] = figi if figi.present?

        log_tagger.tagged(%w[TINKOFF API_INVEST]) do
          @conn.get(Routes::OPERATIONS) do |req|
            req.params = request_params
          end
        end
      end

      # Candles see docs https://tinkoffcreditsystems.github.io/invest-openapi/swagger-ui/#/market/get_market_candles
      # figi      - String
      # from      - DataeTime
      # to        - DateTime
      # interval  - String
      def candles_for_figi(figi, from, to, interval = DEFAULT_INTERVAL)
        log_tagger.tagged(%w[TINKOFF API_INVEST]) do
          request_params = { from: from.rfc3339, to: to.rfc3339, interval: interval, figi: figi }
          @conn.get(Routes::CANDLES) do |req|
            req.params = request_params
          end
        end
      end

      # candles see docs
      # figies    - Array
      # interval  - String
      def candles(figies, interval = DEFAULT_INTERVAL)
        candles = {}
        Async do
          internet = Async::HTTP::Internet.new
          barrier = Async::Barrier.new

          # Spawn an asynchronous task for each topic:
          figies.each do |figi|
            from = figi[:from]
            to = figi[:to] || Date.today
            next if from.blank?

            barrier.async do
              headers = { 'Content-Type' => 'application/json',
                          'Authorization' => "Bearer #{api_token}" }
              request_params = { from: from.rfc3339, to: to.rfc3339, interval: interval, figi: figi }
              candles[figi] = internet.get("#{TINKOFF_API_URL}/#{Routes::CANDLES}", headers, request_params)
            end
          end
          # Ensure we wait for all requests to complete before continuing:
          barrier.wait
        ensure
          internet&.close
        end
      end

      def self.process_request(request); end

      private

      def configure_connection
        @conn = Faraday.new(
          url: TINKOFF_API_URL,
          headers: { 'Content-Type' => 'application/json' }
        ) do |conn|
          conn.request  :authorization, 'Bearer', @api_token
          conn.response :json
          conn.response :logger, Rails.logger, { bodies: false, log_level: :debug }
        end
      end

      def log_tagger
        @log_tagger ||= ActiveSupport::TaggedLogging.new(Rails.logger)
      end
    end
  end
end
