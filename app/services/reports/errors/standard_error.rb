# frozen_string_literal: true

module Reports
  module Errors
    class StandardError < StandardError
      def initialize(msg, report_id)
        @report_id = report_id
        super(msg)
      end
    end
  end
end