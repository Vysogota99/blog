# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'
  devise_for :users, controllers: {
    sessions:       'users/sessions',
    registrations:  'users/registrations'
  }
  mount ActionCable.server => '/cable'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :examples
  resources :chat_rooms
  resources :messages
  resources :reports do
    resources :operations
  end
  resources :posts do
    resources :comments
  end
end
