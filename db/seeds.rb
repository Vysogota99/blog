# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def random_text(n)
  n.times.map { (0...(rand(10))).map { ('a'..'z').to_a[rand(26)] }.join }.join(" ")
end

10.times do |i|
  User.create(email: "user#{i+2}@mail.ru", password: '123456')
end

40.times do |i|
  user = User.find(rand(10)+1)
  next if user.blank?

  Post.create(
    title: random_text(rand(10)).capitalize,
    annotation: random_text(rand(100)).capitalize,
    introduction: random_text(rand(100)).capitalize,
    literature_review: random_text(rand(50)).capitalize,
    main_part: random_text(rand(250)).capitalize,
    conclusions: random_text(rand(150)).capitalize,
    bibliography: random_text(rand(10)).split,
    keywords: random_text(rand(10)).split,
    user_id: user.id
  )
end
