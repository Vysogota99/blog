class CreatePositions < ActiveRecord::Migration[6.1]
  def change
    create_table :positions do |t|
      enable_extension 'hstore' unless extension_enabled?('hstore')

      t.belongs_to :report

      t.string :figi
      t.string :ticker
      t.string :instrumentType
      t.string :name

      t.float :balance

      t.integer :logs

      t.hstore :expectedYield
      t.hstore :averagePositionPrice
      t.timestamps
    end
  end
end
