class CreateReports < ActiveRecord::Migration[6.1]
  def change
    create_table :reports do |t|
      t.belongs_to :user

      t.integer :report_type
      t.integer :status

      t.datetime :start_date
      t.datetime :end_date

      t.binary   :broker_report
      t.timestamps
    end
  end
end
