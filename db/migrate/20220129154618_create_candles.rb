class CreateCandles < ActiveRecord::Migration[6.1]
  def change
    create_table :candles do |t|
      t.belongs_to :report

      t.float :o
      t.float :c
      t.float :h
      t.float :l
      t.float :v
      t.datetime :time
      t.string :interval
      t.string :figi

      t.timestamps
    end
  end
end
