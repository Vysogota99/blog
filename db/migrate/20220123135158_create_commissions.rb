class CreateCommissions < ActiveRecord::Migration[6.1]
  def change
    create_table :commissions do |t|
      t.belongs_to :operation

      t.string :currency
      t.float :value

      t.timestamps
    end
  end
end
