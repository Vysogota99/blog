class CreateTrades < ActiveRecord::Migration[6.1]
  def change
    create_table :trades do |t|
      t.belongs_to :operation
      t.string :trade_id
      t.datetime :date
      t.integer :quantity
      t.float :price

      t.timestamps
    end
  end
end
