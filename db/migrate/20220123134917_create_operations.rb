class CreateOperations < ActiveRecord::Migration[6.1]
  def change
    create_table :operations do |t|
      t.belongs_to :report

      t.string :operation_type
      t.datetime :date
      t.boolean :is_margin_call
      t.string :instrument_type
      t.string :figi
      t.integer :quantity
      t.integer :quantity_executed
      t.float :price
      t.float :payment
      t.string :currency
      t.string :status

      t.timestamps
    end
  end
end
