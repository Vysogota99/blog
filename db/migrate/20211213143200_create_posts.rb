class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.belongs_to :user

      t.string :title
      t.text :body
      t.integer :likes_count
      t.integer :views_count

      t.timestamps
    end
  end
end
