class AddNewPartsToPost < ActiveRecord::Migration[6.1]
  def change
    add_column :posts, :annotation, :string
    add_column :posts, :keywords, :string,  array: true
    add_column :posts, :introduction, :string
    add_column :posts, :literature_review, :string
    add_column :posts, :main_part, :string
    add_column :posts, :conclusions, :string
    add_column :posts, :bibliography, :string,  array: true
  end
end
