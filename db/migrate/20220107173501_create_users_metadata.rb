class CreateUsersMetadata < ActiveRecord::Migration[6.1]
  def change
    create_table :users_metadata do |t|
      t.belongs_to  :user
      t.string      :tinkoff_token
      t.string      :tinkoff_portfolio_id
      t.timestamps
    end
  end
end
